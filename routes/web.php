<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [
    'as' => 'home',
    'uses' => 'PagesController@home',
]);

Route::get('signup', [
    'as' => 'signup',
    'uses' => 'ParticipantsController@create',
]);

Route::post('signup', [
    'as' => 'signup',
    'uses' => 'ParticipantsController@store',
]);

Route::post('pay/{product}', [
    'as' => 'pay/{product}',
    'uses' => 'OrderController@postPayWithStripe',
]);

Route::get('success', [
    'as' => 'success',
    'uses' => 'PagesController@success',
]);

Route::resource('participants', 'ParticipantsController');
