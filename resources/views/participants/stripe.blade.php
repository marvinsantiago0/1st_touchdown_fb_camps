@extends('layouts.master')

@section('content')
<div class="container" style="margin-top: 60px">

    <h1>Participants List</h1>

    <hr>


<div class="modal-body">

<form action="pay.stripe" method="POST" class="donation-form">
{{ csrf_field() }}

<div class="messages">
    <!-- Error messages go here go here -->
</div>



<div class="row">
    <div class="col-sm-6">

        <h2>
            Contact Information
        </h2>


        <div class="row">

            <div class="form-group col-sm-6">
                <label for="input">First Name</label><span class="required_input">*</span>
                <input type="text" class="form-control first-name" id="first-name" name="first-name" placeholder="First Name" required="true">
            </div>

            <div class="form-group col-sm-6">
                <label for="input">Last Name</label><span class="required_input">*</span>
                <input type="text" class="form-control last-name" id="last-name" name="last-name" placeholder="Last Name" required="true">
            </div>

        </div>


        <div class="form-group">
            <label for="input">Email</label><span class="required_input">*</span>
            <input type="email" class="form-control email" id="email" name="email" placeholder="Email" required="true">
        </div>

        <div class="form-group">
            <label for="input">Phone</label><span class="required_input">*</span>
            <input type="text" class="form-control phone" id="phone" name="phone" placeholder="Phone" required="true">
        </div>

        <div class="form-group">
            <label for="input">Address</label><span class="required_input">*</span>
            <input type="text" class="form-control address" id="address" name="address" placeholder="Address" required="true">
        </div>

        <div class="row">

            <div class="form-group col-sm-5">
                <label for="input">City</label><span class="required_input">*</span>
                <input type="text" class="form-control city" id="city" name="city" placeholder="City" required="true">
            </div>

            <div class="form-group col-sm-3">
            <label>State</label><span class="required_input">*</span>
            <select name="state" class="form-control state" required="true">
                <option value="AL">AL</option>
                <option value="AK">AK</option>
                <option value="AZ">AZ</option>
                <option value="AR">AR</option>
                <option value="CA">CA</option>
                <option value="CO">CO</option>
                <option value="CT">CT</option>
                <option value="DE">DE</option>
                <option value="DC">DC</option>
                <option value="FL">FL</option>
                <option value="GA">GA</option>
                <option value="HI">HI</option>
                <option value="ID">ID</option>
                <option value="IL">IL</option>
                <option value="IN">IN</option>
                <option value="IA">IA</option>
                <option value="KS">KS</option>
                <option value="KY">KY</option>
                <option value="LA">LA</option>
                <option value="ME">ME</option>
                <option value="MD">MD</option>
                <option value="MA">MA</option>
                <option value="MI">MI</option>
                <option value="MN">MN</option>
                <option value="MS">MS</option>
                <option value="MO">MO</option>
                <option value="MT">MT</option>
                <option value="NE">NE</option>
                <option value="NV">NV</option>
                <option value="NH">NH</option>
                <option value="NJ">NJ</option>
                <option value="NM">NM</option>
                <option value="NY">NY</option>
                <option value="NC">NC</option>
                <option value="ND">ND</option>
                <option value="OH">OH</option>
                <option value="OK">OK</option>
                <option value="OR">OR</option>
                <option value="PA">PA</option>
                <option value="RI">RI</option>
                <option value="SC">SC</option>
                <option value="SD">SD</option>
                <option value="TN">TN</option>
                <option value="TX">TX</option>
                <option value="UT">UT</option>
                <option value="VT">VT</option>
                <option value="VA">VA</option>
                <option value="WA">WA</option>
                <option value="WV">WV</option>
                <option value="WI">WI</option>
                <option value="WY">WY</option>
            </select>
            </div>

            <div class="form-group col-sm-4">
                <label for="input">Zip</label><span class="required_input">*</span>
                <input type="text" class="form-control zip" id="zip" name="zip" placeholder="Zip" required="true">
            </div>

        </div>

    </div>
    <div class="col-sm-6">

        <h2>
            Your Donation
        </h2>

        <div class="row">
        <div class="col-sm-12">

                <label for="input">Amount</label><span class="required_input">*</span>
                <input type="text" class="form-control amount" id="amount" name="amount" placeholder="Amount" required="true">

        </div>
        </div>

        <h2>
            Credit Card Information
        </h2>

        <div class="form-group">
            <label for="input">Card Number</label><span class="required_input">*</span>
            <input type="text" class="form-control card-number" id="card-number" name="card-number" placeholder="Card Number">
        </div>

        <div class="row">

            <div class="form-group col-sm-3">
                <label for="input">CVC</label><span class="required_input">*</span>
                <input type="text" class="form-control card-cvc" id="card-cvc" name="card-cvc" placeholder="CVC" autocomplete="off" required="true">
            </div>

            <div class="form-group col-sm-5">
            <label>Expiration Date</label><span class="required_input">*</span>
            <select name="card-expiry-month" class="form-control card-expiry-month" required="true">
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04">April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
            </div>

            <div class="form-group col-sm-4">
            <label>Expiration Year</label><span class="required_input">*</span>
            <select name="card-expiry-year" class="form-control card-expiry-year" required="true">
                <option value="2017">2017</option>
                <option value="2018">2018</option>
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
                <option value="2026">2026</option>
                <option value="2027">2027</option>
                <option value="2028">2028</option>
                <option value="2029">2029</option>
                <option value="2030">2030</option>
                <option value="2031">2031</option>
                <option value="2032">2032</option>
                <option value="2033">2033</option>
                <option value="2034">2034</option>
                <option value="2035">2035</option>
            </select>
            </div>

            <br>
            <br>
            <br>
            <br>

            <div class="row">

                <div class="col-sm-8 col-sm-offset-2">

                <button type="submit" class="btn btn-success btn-block">Pay</button>

                </div>
            </div>


    </div>
</div>


</form>


</div>



</div>
@stop
