@extends('layouts.master')

@section('content')
	<div class="container">
		<h1>Campers List</h1>
		<p class="lead">Here's a list of all the campers. <a href="{{ route('participants.create') }}">Add a new one?</a></p>
		<hr>
			<div class="row">
				@foreach($participants as $participant)
				<div class="col-md-4">
					<div class="well">
					<span>
						<h3>{{ $participant->first_name }} {{ $participant->last_name }}</h3>
						
        					<a href="{{ route('participants.show', $participant->id) }}" class="btn btn-info">View More</a>
        					<!-- <a href="{{ route('participants.edit', $participant->id) }}" class="btn btn-primary">Edit Participant</a> -->
    					</span>
					</div>
				</div>
				@endforeach
			</div>
					
	</div>


@stop