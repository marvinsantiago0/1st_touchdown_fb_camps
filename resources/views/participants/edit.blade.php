@extends('layouts.master')

@section('content')
	<div class="container">
		<h1>Edit Participant - Participant Name </h1>
		<p class="lead">Edit this participant below. <a href="{{ route('participants.index') }}">Go back to all participants.</a></p>
		<hr>

		@include('partials.alerts.errors')

		@if(Session::has('flash_message'))
		    <div class="alert alert-success">
		        {{ Session::get('flash_message') }}
		    </div>
		@endif

		{!! Form::model($participant, [
		    'method' => 'PATCH',
		    'route' => ['participants.update', $participant->id]
		]) !!}

		<h5 class="bg-primary">Participant Information</h5>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="first_name">First Name</label>
              <input type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="last_name">Last Name</label>
              <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="address">Address</label>
              <input type="text" id="address" name="address" class="form-control" placeholder="Address">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="city">City</label>
              <input type="text" id="city" name="city" class="form-control" placeholder="City">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="state">State</label>
              <input type="text" id="state" name="state" class="form-control" placeholder="State">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="zipcode">Zip Code</label>
              <input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="Zip Code">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="home_phone">Home Phone</label>
              <input type="text" id="home_phone" name="home_phone" class="form-control" placeholder="(000) 000-0000">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="cell_phone">Cell Phone</label>
              <input type="text" id="cell_phone" name="cell_phone" class="form-control" placeholder="(000) 000-0000">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" id="email" name="email" class="form-control" placeholder="name@example.com">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="guardian_email">Participant / Guardian Email</label>
              <input type="email" id="guardian_email" name="guardian_email" class="form-control" placeholder="name@example.com">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="dob">Date of Birth</label>
              <input type="text" id="dob" name="dob" class="form-control" placeholder="01/01/1990">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="school">School</label>
              <input type="text" id="school" name="school" class="form-control" placeholder="Joe T. Robinson">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="primary_position">Primary Position</label>
              <select name="primary_position" id="primary_position" class="form-control">
                <option value="qb">QB</option>
                <option value="rb">RB</option>
                <option value="wr">WR</option>
                <option value="te">TE</option>
                <option value="ol">OL</option>
                <option value="dl">DL</option>
                <option value="lb">LB</option>
                <option value="s">S</option>
                <option value="cb">CB</option>
                <option value="ath">ATH</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="secondary_position">Secondary Position</label>
              <select name="secondary_position" id="secondary_position" class="form-control">
                <option value="qb">QB</option>
                <option value="rb">RB</option>
                <option value="wr">WR</option>
                <option value="te">TE</option>
                <option value="ol">OL</option>
                <option value="dl">DL</option>
                <option value="lb">LB</option>
                <option value="s">S</option>
                <option value="cb">CB</option>
                <option value="ath">ATH</option>
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="tshirt_size">T-Shirt Size</label>
              <select name="tshirt_size" id="tshirt_size" class="form-control">
                <option value="ys">YS</option>
                <option value="ym">YM</option>
                <option value="yl">YL</option>
                <option value="yxl">YXL</option>
                <option value="sm">S</option>
                <option value="md">M</option>
                <option value="lg">L</option>
                <option value="xl">XL</option>
              </select>
            </div>
          </div>
        </div>
        <hr>
        <h5>Any pre-exisitng conditions that needs to be aware of?</h5>
        <div class="form-group">
          <textarea class="form-control" rows="3" name="med_conditions" placeholder="Please provide details"></textarea>
        </div>
        <hr>
        <h5 class="bg-primary">Parent / Guardian Information</h5>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="parent_first_name">First Name</label>
              <input type="text" id="parent_first_name" name="parent_first_name" class="form-control" placeholder="First Name">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="parent_last_name">Last Name</label>
              <input type="text" id="parent_last_name" name="parent_last_name" class="form-control" placeholder="Last Name">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="parent_phone">Phone</label>
              <input type="text" id="parent_phone" name="parent_phone" class="form-control" placeholder="(000) 000-0000">
            </div>
          </div>
        </div>
        <hr>
        <h5 class="bg-primary">Waiver Information</h5>
        <p>Bacon ipsum dolor amet drumstick chicken andouille shankle venison frankfurter pancetta beef meatball ground round turducken meatloaf tri-tip. Beef pork loin corned beef sirloin pig shoulder pastrami jowl alcatra tail meatloaf. Pork tail burgdoggen brisket strip steak chuck. Venison salami ball tip pork chop short loin. Tenderloin ground round landjaeger meatloaf turducken, ribeye tail swine prosciutto rump chuck chicken t-bone pork belly. Pork loin spare ribs pastrami capicola cow.</p>
        <p>Cow burgdoggen sirloin jerky pork loin boudin shank. Shankle turkey t-bone turducken landjaeger swine short loin. Boudin tri-tip short loin kielbasa picanha jowl pig flank brisket. Shoulder ground round filet mignon salami pastrami prosciutto pancetta alcatra jowl. Alcatra cupim rump pastrami, meatball tongue pork belly fatback porchetta frankfurter bacon pork.</p>
        <div class="checkbox">
          <label>
            <input type="checkbox"> I understand and accept all of the separate event refund policies and/or waiver agreements above.
          </label>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="waiver_signature">Name (or Name of Parent/Guardian if Participant is a minor)</label>
              <input type="text" id="waiver_signature" name="waiver_signature" class="form-control" placeholder="First and Last Name">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="waiver_date">Date</label>
              <input type="date" id="waiver_date" name="waiver_date" class="form-control" placeholder="">
            </div>
          </div>
        </div>
        <hr>
        <h5 class="bg-primary">Emergency Contact</h5>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="emergency_first_name">First Name</label>
              <input type="text" id="emergency_first_name" name="emergency_first_name" class="form-control" placeholder="First Name">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="emergency_last_name">Last Name</label>
              <input type="text" id="emergency_last_name" name="emergency_last_name" class="form-control" placeholder="Last Name">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="emergency_phone">Phone</label>
              <input type="text" id="emergency_phone" name="emergency_phone" class="form-control" placeholder="(000) 000-0000">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="emergency_alt_phone">Alt Phone</label>
              <input type="text" id="emergency_alt_phone" name="emergency_alt_phone" class="form-control" placeholder="(000) 000-0000">
            </div>
          </div>
        </div>
        <hr>

          <div class="text-center">
            <button type="submit" class="btn btn-default">Update</button>
          </div>

        {!! Form::close() !!}
	</div>


@stop