@extends('layouts.master')

@section('content')
<div class="container">
	<h1>{{ $participant->first_name }} {{ $participant->last_name }}</h1>
	<p class="lead">{{ $participant->address }} {{ $participant->city }}, {{ $participant->state }} {{ $participant->zipcode }}</p>
	<p class="lead">Home Phone: {{ $participant->home_phone }} | Cell Phone: {{ $participant->cell_phone }}</p>
	<p class="lead">Email: {{ $participant->email }} | Guardian/Parent Email: {{ $participant->guardian_email }}</p>
	<p class="lead">DOB: {{ $participant->dob }} | School: {{ $participant->school }} </p>
	<p class="lead">Primary Position: {{ $participant->primary_position }} | Secondary Position: {{ $participant->secondary_position }}</p>
	<p class="lead">T-shirt size: {{ $participant->tshirt_size }}</p>
	<p class="lead">Pre-med Conditions: {{ $participant->med_conditions }}</p>
	<hr>
	<h2>Guardian/Parent Information</h2>
	<p class="lead">{{ $participant->parent_first_name }} {{ $participant->parent_last_name }}</p>
	<p class="lead">{{ $participant->parent_phone }}</p>

	<hr>
	<h3>Waiver Information</h3>
	<p class="lead">Waiver Signature: {{ $participant->waiver_signature }} </p>
	<p class="lead">Waiver Date: {{ $participant->waiver_date}}</p>

	<hr>
	<h3>Emergency Contact Information</h3>
	<p class="lead">{{ $participant->emergency_first_name }} {{ $participant->emergency_last_name }}</p>
	<p class="lead">Phone: {{ $participant->emergency_phone }} | Alt Phone: {{ $participant->emergency_alt_phone }}</p>

	<hr>

{{--
	<a href="{{ route('participants.index') }}" class="btn btn-info">Back to all participants</a>
	<a href="{{ route('participants.edit', $participant->id) }}" class="btn btn-primary">Edit participant</a>
	<div class="pull-right">
	    {!! Form::open([
            'method' => 'DELETE',
            'route' => ['participants.destroy', $participant->id]
        ]) !!}
            {!! Form::submit('Delete this participant?', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
	</div>
--}}

</div>
@stop
