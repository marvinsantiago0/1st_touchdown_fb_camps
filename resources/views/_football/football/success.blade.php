@extends('layouts.master')

@section('content')
<section id="section-stripe-pay">
<div class="row">
<div class="col-sm-10 col-sm-offset-1">

@if(session('msg'))
	<div class="alert alert-success" role="alert">
		{{ session('msg') }}
	</div>
@endif

@if(session('error'))
	<div class="alert alert-danger" role="alert">
		{{ session('error') }}
	</div>
@endif

<div class="row">


</div>

</div>
</div>
</section>
@stop
