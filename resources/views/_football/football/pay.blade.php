@extends('layouts.master')


@section('content')
<section id="section-stripe-pay">
<div class="row">
<div class="col-sm-10 col-sm-offset-1">

@if(session('msg'))
	<div class="alert alert-success" role="alert">
		{{ session('msg') }}
	</div>
@endif

@if(session('error'))
	<div class="alert alert-danger" role="alert">
		{{ session('error') }}
	</div>
@endif

<div class="row">
	@foreach ($products as $product)

		<div class="col-sm-5 col-md-5">

			<form action="/pay/{{ $product->id }}" method="POST">
			{{ csrf_field() }}
			{!! Form::hidden('user_id', $user_id) !!}

	    	<div class="thumbnail">
	     		<div class="caption">
			        <h3>{{ $product->name }}</h3>
			        <p>{{ $product->description }}</p>
			        <p>Buy for ${{ substr_replace($product->price, '.', 2, 0) }}</p>

			        <p>
			        	<script
						    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
						    data-key="{{ env('STRIPE_KEY') }}"
						    data-amount="{{ $product->price }}"
						    data-name="quarterback camp"
							data-locale="auto"
							data-currency="usd">
						</script>
			        </p>
	      		</div>
	    	</div>

			</form>

	  	</div>
	@endforeach
</div>

</div>
</div>
</section>
@stop
