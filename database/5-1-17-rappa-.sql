# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.17)
# Database: rappa
# Generation Time: 2017-05-01 21:06:08 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table football_orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `football_orders`;

CREATE TABLE `football_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `football_orders` WRITE;
/*!40000 ALTER TABLE `football_orders` DISABLE KEYS */;

INSERT INTO `football_orders` (`id`, `email`, `product`, `created_at`, `updated_at`)
VALUES
	(1,'sdoe@example.com','product 1','2017-05-01 20:56:44','2017-05-01 20:56:44'),
	(2,'sdoe@example.com','product 1','2017-05-01 21:02:01','2017-05-01 21:02:01'),
	(3,'johndoe@gmail.com','product 1','2017-05-01 21:05:12','2017-05-01 21:05:12');

/*!40000 ALTER TABLE `football_orders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table football_participants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `football_participants`;

CREATE TABLE `football_participants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cell_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guardian_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primary_position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secondary_position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tshirt_size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `med_conditions` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `waiver_signature` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `waiver_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emergency_first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emergency_last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emergency_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `emergency_alt_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `football_participants` WRITE;
/*!40000 ALTER TABLE `football_participants` DISABLE KEYS */;

INSERT INTO `football_participants` (`id`, `first_name`, `last_name`, `address`, `city`, `state`, `zipcode`, `home_phone`, `cell_phone`, `email`, `guardian_email`, `dob`, `school`, `primary_position`, `secondary_position`, `tshirt_size`, `med_conditions`, `parent_first_name`, `parent_last_name`, `parent_phone`, `waiver_signature`, `waiver_date`, `emergency_first_name`, `emergency_last_name`, `emergency_phone`, `emergency_alt_phone`, `created_at`, `updated_at`)
VALUES
	(3,'jonny','doe','1234 Main Street','City','AR','72000','5551234567','5551236','jdoe@example.com','sdoe@example.com','01011990','JTR','qb','qb','ys','none','Sally','Doe','5557899966','Sally Doe','2017-04-30','Sam','Doe','5554567878','5551234578','2017-05-01 04:17:50','2017-05-01 04:17:50'),
	(4,'jimmy','smith','12345 Main St','Bauxite','AR','72014','5017894564','5017894564','johndoe@gmail.com','johndoe@gmail.com','01011990','ftr','qb','qb','ys','none','hanna','smith','5557894561','hanna smith','2017-05-01','frank','smith','5554567896','5551234587','2017-05-01 21:04:52','2017-05-01 21:04:52'),
	(5,'jimmy','smith','12345 Main St','Bauxite','AR','72014','5017894564','5017894564','johndoe@gmail.com','johndoe@gmail.com','01011990','ftr','qb','qb','ys','none','hanna','smith','5557894561','hanna smith','2017-05-01','frank','smith','5554567896','5551234587','2017-05-01 21:05:44','2017-05-01 21:05:44');

/*!40000 ALTER TABLE `football_participants` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table football_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `football_products`;

CREATE TABLE `football_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `football_products` WRITE;
/*!40000 ALTER TABLE `football_products` DISABLE KEYS */;

INSERT INTO `football_products` (`id`, `name`, `description`, `price`, `created_at`, `updated_at`)
VALUES
	(1,'product 1','product',1000,NULL,NULL),
	(2,'product 2','product',2000,NULL,NULL);

/*!40000 ALTER TABLE `football_products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table football_subscriptions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `football_subscriptions`;

CREATE TABLE `football_subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `history`;

CREATE TABLE `history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assets` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `history_type_id_foreign` (`type_id`),
  KEY `history_user_id_foreign` (`user_id`),
  CONSTRAINT `history_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `history_types` (`id`) ON DELETE CASCADE,
  CONSTRAINT `history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table history_types
# ------------------------------------------------------------

DROP TABLE IF EXISTS `history_types`;

CREATE TABLE `history_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `history_types` WRITE;
/*!40000 ALTER TABLE `history_types` DISABLE KEYS */;

INSERT INTO `history_types` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'User','2017-04-11 02:49:13','2017-04-11 02:49:13'),
	(2,'Role','2017-04-11 02:49:13','2017-04-11 02:49:13');

/*!40000 ALTER TABLE `history_types` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2012_12_06_225921_migration_cartalyst_sentry_install_users',1),
	('2012_12_06_225929_migration_cartalyst_sentry_install_groups',1),
	('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot',1),
	('2012_12_06_225988_migration_cartalyst_sentry_install_throttle',1),
	('2013_03_23_193214_update_users_table',1),
	('2013_11_13_075318_create_models_table',1),
	('2013_11_13_075335_create_categories_table',1),
	('2013_11_13_075347_create_manufacturers_table',1),
	('2013_11_15_015858_add_user_id_to_categories',1),
	('2013_11_15_112701_add_user_id_to_manufacturers',1),
	('2013_11_15_190327_create_assets_table',1),
	('2013_11_15_190357_create_licenses_table',1),
	('2013_11_15_201848_add_license_name_to_licenses',1),
	('2013_11_16_040323_create_depreciations_table',1),
	('2013_11_16_042851_add_depreciation_id_to_models',1),
	('2013_11_16_084923_add_user_id_to_models',1),
	('2013_11_16_103258_create_locations_table',1),
	('2013_11_16_103336_add_location_id_to_assets',1),
	('2013_11_16_103407_add_checkedout_to_to_assets',1),
	('2013_11_16_103425_create_history_table',1),
	('2013_11_17_054359_drop_licenses_table',1),
	('2013_11_17_054526_add_physical_to_assets',1),
	('2013_11_17_055126_create_settings_table',1),
	('2013_11_17_062634_add_license_to_assets',1),
	('2013_11_18_134332_add_contacts_to_users',1),
	('2013_11_18_142847_add_info_to_locations',1),
	('2013_11_18_152942_remove_location_id_from_asset',1),
	('2013_11_18_164423_set_nullvalues_for_user',1),
	('2013_11_19_013337_create_asset_logs_table',1),
	('2013_11_19_061409_edit_added_on_asset_logs_table',1),
	('2013_11_19_062250_edit_location_id_asset_logs_table',1),
	('2013_11_20_055822_add_soft_delete_on_assets',1),
	('2013_11_20_121404_add_soft_delete_on_locations',1),
	('2013_11_20_123137_add_soft_delete_on_manufacturers',1),
	('2013_11_20_123725_add_soft_delete_on_categories',1),
	('2013_11_20_130248_create_status_labels',1),
	('2013_11_20_130830_add_status_id_on_assets_table',1),
	('2013_11_20_131544_add_status_type_on_status_labels',1),
	('2013_11_20_134103_add_archived_to_assets',1),
	('2013_11_21_002321_add_uploads_table',1),
	('2013_11_21_024531_remove_deployable_boolean_from_status_labels',1),
	('2013_11_22_075308_add_option_label_to_settings_table',1),
	('2013_11_22_213400_edits_to_settings_table',1),
	('2013_11_25_013244_create_licenses_table',1),
	('2013_11_25_031458_create_license_seats_table',1),
	('2013_11_25_032022_add_type_to_actionlog_table',1),
	('2013_11_25_033008_delete_bad_licenses_table',1),
	('2013_11_25_033131_create_new_licenses_table',1),
	('2013_11_25_033534_add_licensed_to_licenses_table',1),
	('2013_11_25_101308_add_warrantee_to_assets_table',1),
	('2013_11_25_104343_alter_warranty_column_on_assets',1),
	('2013_11_25_150450_drop_parent_from_categories',1),
	('2013_11_25_151920_add_depreciate_to_assets',1),
	('2013_11_25_152903_add_depreciate_to_licenses_table',1),
	('2013_11_26_211820_drop_license_from_assets_table',1),
	('2013_11_27_062510_add_note_to_asset_logs_table',1),
	('2013_12_01_113426_add_filename_to_asset_log',1),
	('2013_12_06_094618_add_nullable_to_licenses_table',1),
	('2013_12_10_084038_add_eol_on_models_table',1),
	('2013_12_12_055218_add_manager_to_users_table',1),
	('2014_01_28_031200_add_qr_code_to_settings_table',1),
	('2014_02_13_183016_add_qr_text_to_settings_table',1),
	('2014_05_24_093839_alter_default_license_depreciation_id',1),
	('2014_05_27_231658_alter_default_values_licenses',1),
	('2014_06_19_191508_add_asset_name_to_settings',1),
	('2014_06_20_004847_make_asset_log_checkedout_to_nullable',1),
	('2014_06_20_005050_make_asset_log_purchasedate_to_nullable',1),
	('2014_06_24_003011_add_suppliers',1),
	('2014_06_24_010742_add_supplier_id_to_asset',1),
	('2014_06_24_012839_add_zip_to_supplier',1),
	('2014_06_24_033908_add_url_to_supplier',1),
	('2014_07_08_054116_add_employee_id_to_users',1),
	('2014_07_09_134316_add_requestable_to_assets',1),
	('2014_07_17_085822_add_asset_to_software',1),
	('2014_07_17_161625_make_asset_id_in_logs_nullable',1),
	('2014_08_12_053504_alpha_0_4_2_release',1),
	('2014_08_17_083523_make_location_id_nullable',1),
	('2014_10_16_200626_add_rtd_location_to_assets',1),
	('2014_10_24_000417_alter_supplier_state_to_32',1),
	('2014_10_24_015641_add_display_checkout_date',1),
	('2014_10_28_222654_add_avatar_field_to_users_table',1),
	('2014_10_29_045924_add_image_field_to_models_table',1),
	('2014_11_01_214955_add_eol_display_to_settings',1),
	('2014_11_04_231416_update_group_field_for_reporting',1),
	('2014_11_05_212408_add_fields_to_licenses',1),
	('2014_11_07_021042_add_image_to_supplier',1),
	('2014_11_20_203007_add_username_to_user',1),
	('2014_11_20_223947_add_auto_to_settings',1),
	('2014_11_20_224421_add_prefix_to_settings',1),
	('2014_11_21_104401_change_licence_type',1),
	('2014_12_09_082500_add_fields_maintained_term_to_licenses',1),
	('2015_02_04_155757_increase_user_field_lengths',1),
	('2015_02_07_013537_add_soft_deleted_to_log',1),
	('2015_02_10_040958_fix_bad_assigned_to_ids',1),
	('2015_02_10_053310_migrate_data_to_new_statuses',1),
	('2015_02_11_044104_migrate_make_license_assigned_null',1),
	('2015_02_11_104406_migrate_create_requests_table',1),
	('2015_02_12_001312_add_mac_address_to_asset',1),
	('2015_02_12_024100_change_license_notes_type',1),
	('2015_02_17_231020_add_localonly_to_settings',1),
	('2015_02_19_222322_add_logo_and_colors_to_settings',1),
	('2015_02_24_072043_add_alerts_to_settings',1),
	('2015_02_25_022931_add_eula_fields',1),
	('2015_02_25_204513_add_accessories_table',1),
	('2015_02_26_091228_add_accessories_user_table',1),
	('2015_02_26_115128_add_deleted_at_models',1),
	('2015_02_26_233005_add_category_type',1),
	('2015_03_01_231912_update_accepted_at_to_acceptance_id',1),
	('2015_03_05_011929_add_qr_type_to_settings',1),
	('2015_03_18_055327_add_note_to_user',1),
	('2015_04_29_234704_add_slack_to_settings',1),
	('2015_05_04_085151_add_parent_id_to_locations_table',1),
	('2015_05_22_124421_add_reassignable_to_licenses',1),
	('2015_06_10_003314_fix_default_for_user_notes',1),
	('2015_06_10_003554_create_consumables',1),
	('2015_06_15_183253_move_email_to_username',1),
	('2015_06_23_070346_make_email_nullable',1),
	('2015_06_26_213716_create_asset_maintenances_table',1),
	('2015_07_04_212443_create_custom_fields_table',1),
	('2015_07_09_014359_add_currency_to_settings_and_locations',1),
	('2015_07_21_122022_add_expected_checkin_date_to_asset_logs',1),
	('2015_07_24_093845_add_checkin_email_to_category_table',1),
	('2015_07_25_055415_remove_email_unique_constraint',1),
	('2015_07_29_230054_add_thread_id_to_asset_logs_table',1),
	('2015_07_31_015430_add_accepted_to_assets',1),
	('2015_09_09_195301_add_custom_css_to_settings',1),
	('2015_09_21_235926_create_custom_field_custom_fieldset',1),
	('2015_09_22_000104_create_custom_fieldsets',1),
	('2015_09_22_003321_add_fieldset_id_to_assets',1),
	('2015_09_22_003413_migrate_mac_address',1),
	('2015_09_28_003314_fix_default_purchase_order',1),
	('2015_10_01_024551_add_accessory_consumable_price_info',1),
	('2015_10_12_192706_add_brand_to_settings',1),
	('2015_10_22_003314_fix_defaults_accessories',1),
	('2015_10_23_182625_add_checkout_time_and_expected_checkout_date_to_assets',1),
	('2015_11_05_061015_create_companies_table',1),
	('2015_11_05_061115_add_company_id_to_consumables_table',1),
	('2015_11_05_183749_image',1),
	('2015_11_06_092038_add_company_id_to_accessories_table',1),
	('2015_11_06_100045_add_company_id_to_users_table',1),
	('2015_11_06_134742_add_company_id_to_licenses_table',1),
	('2015_11_08_035832_add_company_id_to_assets_table',1),
	('2015_11_08_222305_add_ldap_fields_to_settings',1),
	('2015_11_15_151803_add_full_multiple_companies_support_to_settings_table',1),
	('2015_11_26_195528_import_ldap_settings',1),
	('2015_11_30_191504_remove_fk_company_id',1),
	('2015_12_21_193006_add_ldap_server_cert_ignore_to_settings_table',1),
	('2015_12_30_233509_add_timestamp_and_userId_to_custom_fields',1),
	('2015_12_30_233658_add_timestamp_and_userId_to_custom_fieldsets',1),
	('2016_01_28_041048_add_notes_to_models',1),
	('2016_02_19_070119_add_remember_token_to_users_table',1),
	('2016_02_19_073625_create_password_resets_table',1),
	('2016_03_02_193043_add_ldap_flag_to_users',1),
	('2016_03_02_220517_update_ldap_filter_to_longer_field',1),
	('2016_03_08_225351_create_components_table',1),
	('2016_03_09_024038_add_min_stock_to_tables',1),
	('2016_03_10_133849_add_locale_to_users',1),
	('2016_03_10_135519_add_locale_to_settings',1),
	('2016_03_11_185621_add_label_settings_to_settings',1),
	('2016_03_22_125911_fix_custom_fields_regexes',1),
	('2016_04_28_141554_add_show_to_users',1),
	('2016_05_16_164733_add_model_mfg_to_consumable',1),
	('2016_05_19_180351_add_alt_barcode_settings',1),
	('2016_05_19_191146_add_alter_interval',1),
	('2016_05_19_192226_add_inventory_threshold',1),
	('2016_05_20_024859_remove_option_keys_from_settings_table',1),
	('2016_05_20_143758_remove_option_value_from_settings_table',1),
	('2016_06_01_140218_add_email_domain_and_format_to_settings',1),
	('2016_06_22_160725_add_user_id_to_maintenances',1),
	('2016_07_13_150015_add_is_ad_to_settings',1),
	('2016_07_14_153609_add_ad_domain_to_settings',1),
	('2016_07_22_003348_fix_custom_fields_regex_stuff',1),
	('2016_07_22_054850_one_more_mac_addr_fix',1),
	('2016_07_22_143045_add_port_to_ldap_settings',1),
	('2016_07_22_153432_add_tls_to_ldap_settings',1),
	('2016_07_27_211034_add_zerofill_to_settings',1),
	('2016_08_02_124944_add_color_to_statuslabel',1),
	('2016_08_04_134500_add_disallow_ldap_pw_sync_to_settings',1),
	('2016_08_09_002225_add_manufacturer_to_licenses',1),
	('2016_08_12_121613_add_manufacturer_to_accessories_table',1),
	('2016_08_23_143353_add_new_fields_to_custom_fields',1),
	('2016_08_23_145619_add_show_in_nav_to_status_labels',1),
	('2016_08_30_084634_make_purchase_cost_nullable',1),
	('2016_09_01_141051_add_requestable_to_asset_model',1),
	('2016_09_02_001448_create_checkout_requests_table',1),
	('2016_09_04_180400_create_actionlog_table',1),
	('2016_09_04_182149_migrate_asset_log_to_action_log',1),
	('2016_09_19_235935_fix_fieldtype_for_target_type',1),
	('2016_09_23_140722_fix_modelno_in_consumables_to_string',1),
	('2016_09_28_231359_add_company_to_logs',1),
	('2016_10_14_130709_fix_order_number_to_varchar',1),
	('2016_10_16_015024_rename_modelno_to_model_number',1),
	('2016_10_16_015211_rename_consumable_modelno_to_model_number',1),
	('2016_10_16_143235_rename_model_note_to_notes',1),
	('2016_10_16_165052_rename_component_total_qty_to_qty',1),
	('2016_10_19_145520_fix_order_number_in_components_to_string',1),
	('2016_10_27_151715_add_serial_to_components',1),
	('2016_10_27_213251_increase_serial_field_capacity',1),
	('2016_10_29_002724_enable_2fa_fields',1),
	('2016_10_29_082408_add_signature_to_acceptance',1),
	('2016_11_01_030818_fix_forgotten_filename_in_action_logs',1),
	('2016_11_13_020954_rename_component_serial_number_to_serial',1),
	('2016_11_16_172119_increase_purchase_cost_size',1),
	('2016_11_17_161317_longer_state_field_in_location',1),
	('2016_11_17_193706_add_model_number_to_accessories',1),
	('2016_11_24_160405_add_missing_target_type_to_logs_table',1),
	('2016_12_07_173720_increase_size_of_state_in_suppliers',1),
	('2016_08_25_001434_create_participants_table',2);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table permission_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_foreign` (`permission_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`)
VALUES
	(1,1,2);

/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`id`, `name`, `display_name`, `sort`, `created_at`, `updated_at`)
VALUES
	(1,'view-backend','View Backend',1,'2017-04-11 02:49:13','2017-04-11 02:49:13');

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table role_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;

INSERT INTO `role_user` (`id`, `user_id`, `role_id`)
VALUES
	(1,1,1),
	(2,2,2),
	(3,3,3);

/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `all` tinyint(1) NOT NULL DEFAULT '0',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `all`, `sort`, `created_at`, `updated_at`)
VALUES
	(1,'Administrator',1,1,'2017-04-11 02:49:13','2017-04-11 02:49:13'),
	(2,'Executive',0,2,'2017-04-11 02:49:13','2017-04-11 02:49:13'),
	(3,'User',0,3,'2017-04-11 02:49:13','2017-04-11 02:49:13');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`)
VALUES
	('miOcgiGEV8D0Wl5WtoDi4cv76y1GuwKN6Sa8ZGIb',6,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36','ZXlKcGRpSTZJalJVUXpjelRUaHZaRXBoWjJZeE5sWk5WSE5NYWxFOVBTSXNJblpoYkhWbElqb2lXa1JQZDJaRFlsYzNlakJjTDNWVVNtSnBRVTkwYVZSTWNWVjVia05uU1ROWk1FWkRjMEU0WTI5alZtbElXVk00WjFWNlZFNWliVnBIYld4MFlqSTRkVUpYUlV0SlZEUldURU5GZUZWY0wwUm1Uelp5YlRNelNtZEtTbFk1Ym01RVpGWm1TbWMzUjJ0RGNFdGFNRUpYYVRWVlFtVmhPRTV0UTJONldGSlVkbmRRVkZSd2MyYzNaV1JQVjFBNWFXeHZVMkY1TlRsRVMxUlljRFpWZEVvcmNVUm9ZV3d5Y3psVU5XOVRkRVZqZGpWelRXWnhXbk5DYjNCT1dFSjZWRmhCUkZobFVITmtPV3RSZG1WTWFVSnFVMnhNYW1KcVIxaHhjVTVTY2t0YVRGWnBVV1IyVTFaWGNqbENjVEpyTW5ORmNTdFlRbkEzWEM5TVNHZFVUekpTVGs0MFNpdExXbVpNYURCUWFtazVkMnB2Y2psV0syaHlWbGg0WjB0RWRUWjFlSEJOY0ZoaFFuRTNiM2hSYldaTlpHbFFUbXhMZGpoR2NuTllObTFVZFdwdE1sZDVWMmw1Y0dJME5HcGxXblpJTkVoTWFWZ3lNa2RUTkRNd1QzQjZjSGh4TlZGR1NVVnJUVWRUVFN0M1JFMUZNSEJaZWxsTk9VdExNRU5QU2s1SlJTdDVlSGxpWkU1dVNFZHFOa2hjTHl0dk1qSkxTa3hLYVV4TGNrZE9VVE5WYkhreGFXbFNVakUxWTJwVmJIZzBSWEJVYlU1Rldsd3ZOV1ExYURObmFVTktlRVp2Ykd4U05GWk5aalZuU1RCQ2FrVlFWREJ2WjJwRFlWQXJUblZOWEM5SGQzcERkSGg2ZWpOeFZYZEpTamRUZW1rd1FsTlJOWHBRTVV0UFFYVXdTemxZWEM5c2IyMXBiMmRwTVRjMVRHVlhSSGNyZURRMFF6QkxVRmh3TjNSQlZYTm1PVXRGUlRFd1IwZGxjVXRLVDJKU2JURjVYQzlpU0hab2Eyb3phVXM1YkV0UmFuWkZNVlZDY1RZeWVtUmtiRnd2VWxOV1IxTkxSV3hJVjJsM09GWXhabVFyVlZGeE0yMUZVMUJzVkRKM00yRjNhbXB1VUVKemEwTXhiMFZuVTJOdFRFNDNOVnd2TkhWY0wwdzFja2RWU2psRVlYWmhhR3BuY25GNEt5dGpSMlZaVkVsWmIyWnNhMDFjTDJaUWFuSTBiMEU5SWl3aWJXRmpJam9pWXpNeVltWmpZVEppWkRJeE1UTTVNRGN3TUdFMk5XRTJOMkppTkRjM1pqZGpZbVkwT1RBellUa3haREV6TlRjeE9HSTFOVGd6T1RVMk9EQTBNVFE1WkNKOQ==',1493652124),
	('xFU4eh1rn6BpFFzwGRsXJ6EDb5xL66XP3UgVd61o',1,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36','ZXlKcGRpSTZJblZEV2pGdVlXaFRVek0wVjJGeFVWbHNaSGwzYUVFOVBTSXNJblpoYkhWbElqb2lTVWxOZEdOTFZFMXZlVkI1WmpSUVpHZFFZV2x6YmtoTVlYSnRWRlU0UlVkVVNFaFZhSGRPY1ZGeVVXbzBabGwyU20xdE1EUXlRVUZQVjFSbFVGTTBZM0JvYTFZd2FVbHZXa2R2U1ZWNmJFdFpUMjVQTWs4MVVGbFVSbkZDT0d4TmVFWjRTM0IzU1U5QlZVWjVVVm96YUZKemJHNWhkV1J6Vm5FNVZYVnJTQ3RLY1VsWlVEZE5kVUpKVm1WQ1ZuRlhOMHg0WkVkcWRDdEZZamhYV1ZGclp6WnpkSE16Wm5oWWVEQnZUV3hpVmxOVGFUTmtSMVpPWmtGSGQzaFFVM0o2TjNaWU1TdFlaV3hwWldsa09EWjFiVk5FUkVwWldVTldTVXREY2pGb2FFTkphVWhvUVV0aGVHbEtNaXRPZGpScVNtMUJiVGxJUkVGMWNHSmlWRWRqTUUxeVpqUnlNQ3RqWldWdlQyMWFXalkzY1dGS2EzSjFabkpwVUVaSlVreFNXVFJ2UkdSd2VtZG1PRUpMV1VGMFQwbGxlbVk0Vm5aNVdYbFJSVVZrUlVKblRqaEVRbVJ3UkhNMFhDOUthbEJ1YVdOM1lqaFNORFUwVFV4UlJHMW5ZVlZPWWt4R00zRm5TbXhoUm1aR04waGFYQzljTDIxRmJHVmtlVEJxWldoVU4zUlJUR1pHT0d4UmNtOU5RMVpTVTFGVVRqUm1OV2hIWjB3MmJWbFFOalZEVWtaR1Nra3hiREV6UzIxS09XUjBhbXBEV2t0YVNWZDFVR3AxVkVWVGVrcENNeUlzSW0xaFl5STZJamRpTVRRM1kyUTJaVFl6TTJNeVpHUmxOamhrTjJSak1EWTRPRFUzWVRobU5EYzBPVEUwTXpBMU5EaGlNbVpqTm1Zd016Vm1aV05sTUdWa09HTmhZbVFpZlE9PQ==',1493669748),
	('Yt0AIoGjG4TJWknpzvVNwvOCx3LhqJK0AnpBlWtW',1,'127.0.0.1','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36','ZXlKcGRpSTZJbFptZVRSTFRrWkVibkZSTlVJM1JtMW9NVGczWm5jOVBTSXNJblpoYkhWbElqb2lVVkJVZVVoUk9XTlNaMGt3WkVJMVZITkVYQzlsU0VRNFpEVTBLMjV2U2xrMlExbFVTbkpWWEM5VlJVdEZlSGx6V0hsYU5VTTFhMEZ5TVdOelpUa3daVlJrVG1WV1ZFWjRaVXBsZVhoRFVXNTFhMjF6TkhOV2IzZzJkekl5VEZWeWMwMUliRzFXTlROMGRHeFdOWFI2YWxwNGNXazRSVmd3YUdodlpGa3dXR2hEYTB0R1pWUnVaMUF4WmtvcmRUbDBSbWMxZVZwSk5ucDFNSGx0TkhsRWIxbHFNV0pZZUROV2RVVjZWV00xWlhSbGIxSnNRbTkwWjNCV01VOU1Sak5uZGtORFkwaElaa1U1YXlzMU5GWXhjVTVoUkZoQ2NFNTNZVVF5Wlc5MmF6Wk1UMDFPWm1OMVMzVjFNVEF3Y3pCWU5UQlNUMDFrV0hWa1RVeFdUbXh6Ym5saVlqVTVXalpVVkhOWllrUk1NV0UwYmpsSGMzTlVjR04wTkVkUE5EZFNPSE5RWVV0MmNGSkpVa2wyWjFoV1UzbzVSbXAyTWpSS1NtTmFhVlZzZDBaVmQzSlNXRWNyVjFKeVdUbFRLMFY1UkZwQ1EySm9VVWd5WjJwWGFISTVWMUExYUV0MGJrRmthR2hYVmxOeGVsZFpTbGQ2TW5aMlRtbGphSGRCZERGR2JXUllNWFZJTlUxWFNGQjFjbEJoU2pGS1NWSTJVbXhRWkhobE1FaFdYQzltY2xnNU5ERjVkV2xZYm5WWFkySXdjelJGYUdSMk1GWktZbTl0U1U1SU5YSkpVa1l5YUZ3dmRUZzFjRGR2ZG5SV1duSmhkVkEwV1ZOblBUMGlMQ0p0WVdNaU9pSXdaRGRsT0RjMk9HWTVabVk1TXpJMk16TmtNVGd6WXpVNE56WmtNamhoTW1ZMU5EWXpOV1kxWVdSa1ltRTJNemRqWm1RNE1UYzNPR1l5WlRObE1qTTBJbjA9',1493652266);

/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table social_logins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `social_logins`;

CREATE TABLE `social_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `social_logins_user_id_foreign` (`user_id`),
  CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table throttle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `throttle`;

CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `confirmation_code`, `confirmed`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`)
VALUES
	(1,'Admin Istrator','admin@admin.com','$2y$10$XllPXR0Q8mzVSZYH4IbIAOVnwp2FsTKvLAWCeEJR9sZX/OihD0Vny',1,'40edf82d4b5fd2ab69132be5318b9b6a',1,'bxbCxIXWhQRudKngLmqlOVJ4xrLydbqkCQorztMvRYMUEziuXgWbFkWyCzoO','2017-04-11 02:49:13','2017-04-30 15:01:45',NULL,'cus_AZH0FPYQklZrx2',NULL,NULL,NULL),
	(2,'Backend User','executive@executive.com','$2y$10$YWXxTihaWCUQ.S.NOkul1eAfInkQy6WsEYnIpyD/b6PHRrkEhbR0u',1,'28f8fc0905d3616ed53f142df862506d',1,NULL,'2017-04-11 02:49:13','2017-04-11 02:49:13',NULL,NULL,NULL,NULL,NULL),
	(3,'Default User','user@user.com','$2y$10$vI3gbFxLtaXFHpCZYJfdwOjd89PauWx4OWXPc0oZ1.quxM0hSJ2Eu',1,'49dd93a8658fe588a6528cc9ede27238',1,NULL,'2017-04-11 02:49:13','2017-04-11 02:49:13',NULL,NULL,NULL,NULL,NULL),
	(6,'sDoe','sdoe@example.com','$2y$10$mVN9G9VFQFCYCkNHo9wP2uXv14xOkUQ5c2Qdm3fF.HW9sknGmPQPa',1,'1386070c0e0ee71625150579c75f005e',1,'hZWnuvXLlLE1Av1i6EseVfHBkttcQzQHrF9firKRuFPXXq1eiHq1TnkLKQ3l','2017-05-01 04:17:51','2017-05-01 04:18:28',NULL,'cus_AZfK1C6yrqLmRQ',NULL,NULL,NULL),
	(7,'hSmith','johndoe@gmail.com','$2y$10$xj8Ilz0VMTnVU69fVVI50OQ8qjZVpSQAPGAWEc7g0X02fMmVtzoUy',1,NULL,0,NULL,'2017-05-01 21:04:52','2017-05-01 21:05:12',NULL,'cus_AZvYTU4ukOd3Mv',NULL,NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
