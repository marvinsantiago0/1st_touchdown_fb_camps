<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('zipcode');
            $table->string('home_phone');
            $table->string('cell_phone');
            $table->string('email');
            $table->string('guardian_email');
            $table->string('dob');
            $table->string('school');
            $table->string('height');
            $table->string('weight');
            $table->string('parent_first_name');
            $table->string('parent_last_name');
            $table->string('parent_phone');
            $table->string('waiver_signature');
            $table->string('waiver_date');
            $table->string('emergency_first_name');
            $table->string('emergency_last_name');
            $table->string('emergency_phone');
            $table->string('emergency_alt_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
