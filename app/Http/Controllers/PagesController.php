<?php

namespace App\Http\Controllers;

class PagesController extends Controller
{
    public function home()
    {
        return view('pages.home');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function success()
    {
        return view('_football.football.success');
    }

}
