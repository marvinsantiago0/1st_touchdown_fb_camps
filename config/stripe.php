<?php

return [

    'stripe' => [
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    // Enable test mode (not require HTTPS)
    'test-mode' => true,

    // Where to send upon successful donation (must include http://)
    'thank-you' => 'http://www.robinsonchampioncenter.org/thank-you.php',

    // Who the email will be from.
    'email-from' => 'crichter@dntmedia.com',

    // Who should be BCC'd on this email. Probably an administrative email.
    'email-bcc' => 'crichter@dntmedia.com',

    // Subject of email receipt
    'email-subject' => 'Thank you for your donation!',

    // Email message. %name% is the donor's name. %amount% is the donation amount
    'email-message' => "Dear %name%,\n\nThank you for your donation of %amount%. We rely on the financial support from people like you to keep our cause alive. Below is your donation receipt to keep for your records.",

];
